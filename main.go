package main

import (
	"context"
	"log"
	"os"
	"strconv"

	"github.com/barrydevp/codeatest-runner-core/dispatcher"
	"github.com/barrydevp/codeatest-runner-core/puller"
	"github.com/barrydevp/codeatest-runner-core/runner"
	"github.com/barrydevp/codeatest-runner-core/server"
)

const DEFAULT_PORT = "5002"

var RUNNER_NAME string
var PORT string
var BUCKET_SIZE int64

func init() {
	envPORT := os.Getenv("PORT")

	if envPORT == "" {
		PORT = DEFAULT_PORT
		log.Printf("DEFAULT_PORT %s is set.\n", DEFAULT_PORT)
	} else {
		PORT = envPORT
	}

	RUNNER_NAME = os.Getenv("RUNNER_NAME")
	if RUNNER_NAME == "" {
		RUNNER_NAME = "Python3.8"
	}

	bucketSize := os.Getenv("CONCURRENCY")

	if bucketSize == "" {
		BUCKET_SIZE = 1
	} else {
		vBucketSize, err := strconv.ParseInt(bucketSize, 10, 64)

		if err != nil {
			log.Println("parse BUCKET_SIZE error, using default BUCKETSIZE = 1")
			BUCKET_SIZE = 1
		} else {
			BUCKET_SIZE = vBucketSize
		}
	}
}

func main() {
	log.Printf("[RUNNER::%s] UP AND RUNNING...\n", RUNNER_NAME)

	ctx := context.Background()

	PythonRunner := runner.Runner{
		Name:    RUNNER_NAME,
		State:   "created",
		Command: "python",
	}

	PythonPuller := puller.Puller{
		Language:   "py",
		BucketSize: BUCKET_SIZE,
	}

	PythonDispatcher := dispatcher.Dispatcher{
		Name:      RUNNER_NAME,
		Runner:    &PythonRunner,
		Puller:    &PythonPuller,
		Ctx:       ctx,
		IsRunning: false,
		Delay:     10,
	}

	PythonDispatcher.Init()

	go PythonDispatcher.Run()

	app := server.HttpServer{Dispatcher: &PythonDispatcher, PORT: PORT}

	app.ListenAndServe()

	log.Printf("[RUNNER::%s] DOWN BYE BYE...\n", RUNNER_NAME)
}
